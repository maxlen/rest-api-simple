<?php

namespace app\components\behaviors;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class CachedBehavior extends Behavior
{
    const FUNC_DEL = 'deleteCache';

    public $cacheId;

    public function events(): array
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => self::FUNC_DEL,
            ActiveRecord::EVENT_AFTER_UPDATE => self::FUNC_DEL,
            ActiveRecord::EVENT_AFTER_DELETE => self::FUNC_DEL,
        ];
    }

    public function deleteCache(): void
    {
        Yii::$app->cache->delete($this->cacheId);
    }
}