<?php

use app\helpers\Def;
use yii\db\Migration;

/**
 * Class m180630_054251_create_table_posts
 */
class m180630_054251_create_table_posts extends Migration
{
    private $table = 'posts';
    private $optString = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey()->unsigned(),
            'category_id' => $this->integer()->unsigned()->notNull(),
            'name' => $this->string(255)->notNull(),
            'text' => $this->text(),
            'created_at' => $this->dateTime()->defaultValue(Def::DTIME_ZERO),
            'updated_at' => $this->dateTime()->defaultValue(Def::DTIME_ZERO),
        ], $this->optString);

        $this->createIndex('category_id', $this->table, 'category_id');
        $this->createIndex('updated_at', $this->table, 'updated_at');
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
