<?php

use app\helpers\Def;
use yii\db\Migration;

/**
 * Class m180630_054229_create_table_categories
 */
class m180630_054229_create_table_categories extends Migration
{
    private $table = 'categories';
    private $optString = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->text()->notNull(),
            'cnt_posts' => $this->integer()->unsigned()->defaultValue(0),
            'created_at' => $this->dateTime()->defaultValue(Def::DTIME_ZERO),
            'updated_at' => $this->dateTime()->defaultValue(Def::DTIME_ZERO),
        ], $this->optString);

        $this->createIndex('updated_at', $this->table, 'updated_at');
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
