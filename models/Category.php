<?php

namespace app\models;

use app\components\behaviors\CachedBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\HttpException;

/**
 * This is the model class for table "categories".
 *
 * @property int $id
 * @property string $name
 * @property int $cnt_posts
 * @property string $created_at
 * @property string $updated_at
 */
class Category extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories';
    }

    public function behaviors()
    {
        parent::behaviors();
        return [
            'CachedBehavior' => [
                'class' => CachedBehavior::class,
                'cacheId' => 'categories',
            ],
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
            [['cnt_posts'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'cnt_posts' => 'Cnt Posts',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function find()
    {
        return Yii::$app->cache->getOrSet('categories', function () {
            return parent::find();
        });
    }

    public function getPosts()
    {
        return $this->hasMany(Post::class, ['category_id' => 'id']);
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        if (Post::deleteAll('category_id = :category_id', [':category_id' => $this->id]) === false) {
            throw new HttpException(500, "Can't delete posts from this category");
        }

        return true;
    }

    public function updateCntPosts(Category $category): bool
    {
        $category->cnt_posts = (new Post)->getCntOfPostsByCategoryId($category->id);

        if ($category->save()) {
            return true;
        }

        return false;
    }
}
