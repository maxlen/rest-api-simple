### Install
    
Clone

    git clone git@bitbucket.org:maxlen/rest-api-simple.git 

### Docker
    
Start the container

    docker-compose up -d
    
Run couple commands:

    docker ps
    docker exec -it <containerId> php yii migrate

You can then access the application through the following URL:

    http://127.0.0.1:8000


### Postman

Collection: https://www.getpostman.com/collections/47690681d8a462ee9cd6

    GET: http://127.0.0.1:8000/category(post)
    GET: http://127.0.0.1:8000/category(post)/<1>
    POST: http://127.0.0.1:8000/category(post) // data
    PUT: http://127.0.0.1:8000/category(post)/<id> // data
    DELETE: http://127.0.0.1:8000/category(post)/<id>
    

TESTING
-------

Test Flow:

- `create category1`
- `create post1`
- `get categories`
- `update category1`
- `get category1`
- `get posts`
- `update post1`
- `get post1`
- `create post2`
- `delete post2`
- `delete category1` // with posts from category1

Tests can be executed by running

```
docker exec -it <containerName> codecept run api --debug
```


