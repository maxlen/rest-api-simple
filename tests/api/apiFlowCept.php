<?php

namespace tests\api;

use ApiCategoryLogic;
use ApiPostLogic;
use ApiTester;

$i = new ApiTester($scenario);

// create new category
$category = json_decode(
    (new ApiCategoryLogic)->post(
        $i,
        ['name' => 'category name']
    )
);

// create new post
$post = json_decode(
    (new ApiPostLogic)->post(
        $i,
        [
            'category_id' => $category->id,
            'name' => 'post name',
            'text' => 'post text'
        ]
    )
);

(new ApiCategoryLogic)->get($i);
(new ApiCategoryLogic)->put($i, $category->id, ['name' => 'changed name of category']);
(new ApiCategoryLogic)->get($i, $category->id);

(new ApiPostLogic)->get($i);
(new ApiPostLogic)->put($i, $post->id, ['name' => 'changed name of post', 'text' => 'changed text']);
(new ApiPostLogic)->get($i, $post->id);

// create new post
$postNew = json_decode(
    (new ApiPostLogic)->post(
        $i,
        [
            'category_id' => $category->id,
            'name' => 'post name',
            'text' => 'post text'
        ]
    )
);


(new ApiPostLogic)->delete($i, $postNew->id);
(new ApiCategoryLogic)->delete($i, $category->id);
