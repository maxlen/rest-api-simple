<?php

class ApiCategoryLogic extends AbstractApiLogic
{
    public function __construct()
    {
        $this->setItemName('category');
        $this->setTemplate([
            'id' => 'integer',
            'name' => 'string',
            'created_at' => 'string',
            'updated_at' => 'string',
        ]);
    }
}