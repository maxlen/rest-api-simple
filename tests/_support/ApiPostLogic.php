<?php

class ApiPostLogic extends AbstractApiLogic
{
    public function __construct()
    {
        $this->setItemName('post');
        $this->setTemplate([
            'id' => 'integer',
            'name' => 'string',
            'text' => 'string',
            'created_at' => 'string',
            'updated_at' => 'string',
        ]);
    }
}