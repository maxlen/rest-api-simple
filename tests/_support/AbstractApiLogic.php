<?php

use Codeception\Util\HttpCode;

abstract class AbstractApiLogic
{
    const BASE_URL = 'http://172.19.0.3/';

    private $template;
    private $itemName;

    public function post(ApiTester $i, array $data): string
    {
        $url = $this->getUrl($this->itemName);
        $i->comment('Check ' . __METHOD__ . ': ' . $url . '?' . http_build_query($data));

        $i->sendPOST($url, $data);
        $i->seeResponseCodeIs(HttpCode::CREATED);
        $i->seeResponseIsJson();
        return $i->grabResponse();
    }

    public function get(ApiTester $i, $id = null): string
    {
        $url = $this->getUrl($this->itemName, $id);
        $i->comment('Check ' . __METHOD__ . ': ' . $url);

        $i->sendGET($url);
        $i->seeResponseCodeIs(HttpCode::OK);
        $i->seeResponseIsJson();
        $i->canSeeResponseIsJson();

        if (!is_null($id)) {
            $i->seeResponseMatchesJsonType($this->template);
        } else {
            $i->seeResponseContainsJson([]);
        }

        return $i->grabResponse();
    }

    public function put(ApiTester $i, int $id, array $data): string
    {
        $url = $this->getUrl($this->itemName, $id);
        $i->comment('Check ' . __METHOD__ . ': ' . $url);

        $i->sendPUT($url, $data);
        $i->seeResponseCodeIs(HttpCode::OK);
        $i->seeResponseIsJson();
        $i->canSeeResponseIsJson();
        return $i->grabResponse();
    }

    public function delete(ApiTester $i, int $id): void
    {
        $url = $this->getUrl($this->itemName, $id);
        $i->comment('Check ' . __METHOD__ . ': ' . $url);

        $i->sendDELETE($url);
        $i->seeResponseCodeIs(HttpCode::NO_CONTENT);
    }

    private function getUrl(string $itemName, $id = null): string
    {
        $param = !is_null($id) ? "/{$id}" : '';
        return self::BASE_URL . $itemName . $param;
    }

    protected function setItemName(string $itemName): bool
    {
        return (bool)$this->itemName = $itemName;
    }

    protected function setTemplate(array $template): void
    {
        $this->template = $template;
    }
}